#include <iostream>
#include <time.h>
#include <fstream>
#include <stdlib.h>

int N = 2;
int P;

int liczbaWszystkichWartosci = 500;
int liczbaWartosciTestowych = 100;

double teta = 0.003;
using namespace std;

int f(double u) {
	if (u <= 0)
		return 0;
	else
		return 1;
}

int neuron(double *x, double*w) {
	double u = 0;
	for (int i = 0; i < N+1; i++) {
		u += x[i] * w[i];
	}
	u = f(u);
	
	return (int)u;
}

bool istnieje(int *tab, int i) {
	for (int j = 0; j < i;j++)
	if (tab[j] == tab[i])
		return true;

	return false;
}

bool przeszukaj(int *tab, int rozmiar, int wartosc) {
	for (int i = 0; i< rozmiar; i++) {
		if (tab[i] == wartosc)
			return true;
	}
	return false;
}


int main() {
	
	ifstream plik;
	
	int blad = 1;
	int epoka = 0;
	
	P = liczbaWszystkichWartosci - liczbaWartosciTestowych;
	int *index = new int[P];

	plik.open("2D.txt");

	if (!plik.is_open()) {
		cout << "blad otwarcia pliku";
	}

	srand(time(NULL));

	double *w = new double[N+1];
	double **x = new double*[P];
	double **wartosciTestowe = new double*[P];
	for (int i = 0; i < N+1; i++) w[i] = 2*(rand() / (double) RAND_MAX)-1;
	
	for (int i = 0; i < P; i++) {
		x[i] = new double[N + 1];
		x[i][0] = 1;
		wartosciTestowe[i] = new double[N+1];
	}

	for (int i = 0; i < P; i++) {
		do {
			index[i] = rand() % liczbaWszystkichWartosci;
		} while (istnieje(index, i));

	}

	
	int *klasa = new int[P];
	int *klasaTest = new int[liczbaWartosciTestowych];
	
	int IndexTest = 0, indexTab = 0;
	
	
	for (int i = 0; i<liczbaWszystkichWartosci; i++) {
		if (przeszukaj(index, P, i)) {			//sprawdzamy czy w tablicy z wylosowanymi indeksami dla plików do obliczeñ znajduje sie aktualnie testowany wiersz 
			for (int j = 1; j<N+1; j++)
				plik >> x[indexTab][j];		//zapis z pliku do tablicy punktów do obliczeń
			plik >> klasa[indexTab];
			indexTab++;
		} else {									//aktualny wiersz pliku zawiera punkt testowy, nie zostal wylosowany do tablicy indeks
			wartosciTestowe[IndexTest][0] = 1;		//bias=1
			for (int j = 1; j<N+1; j++)
				plik >> wartosciTestowe[IndexTest][j];	//zapis z pliku do tablicy z punktam testowymi
			plik >> klasaTest[IndexTest];
			IndexTest++;
		}
	}
 

	for (int i = 0; i < N+1; i++) { 
		cout << w[i] << endl;
	}
		
	
	while (blad!=0) {
			blad = 0;
			for (int j = 0; j < P; j++) {
				int temp = neuron(x[j], w);
				if (temp == klasa[j])continue;
				else {
					if (klasa[j] == 0)for (int k = 0; k < N + 1; k++) w[k] -= teta*x[j][k];
					if (klasa[j] == 1)for (int k = 0; k < N + 1; k++) w[k] += teta*x[j][k];
				}
			}

			for (int j = 0; j < P; j++) {
				int temp1 = neuron(x[j], w);
				if (temp1 != klasa[j])
					blad++;
			}
			cout << "epoka " << epoka << " bladow " << blad<<endl;
			epoka++;
		}
	
		for (int j = 0; j < liczbaWartosciTestowych; j++) {
			int temp1 = neuron(wartosciTestowe[j], w);
			if (temp1 != klasaTest[j])
				blad++;
		}
		
		cout << "\nWynik po " << epoka << " epokach:\nparam. a = " << -w[1] / w[2] << "\nparam. b = " << -w[0] / w[2] << endl << endl;
		cout << "Testowanie " << liczbaWartosciTestowych << " wartosci";
		cout << "\nBlad wartosci testowych wynosi: " << blad / ((double)liczbaWartosciTestowych) * 100 << "% (" << blad << ")" << endl;

	return 0;
}


